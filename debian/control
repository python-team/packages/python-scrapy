Source: python-scrapy
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Ignace Mouzannar <mouzannar@gmail.com>, Andrey Rakhmatullin <wrar@debian.org>
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-bash-completion,
 dh-sequence-python3,
 dh-sequence-sphinxdoc <!nodoc>,
 python3-all,
Build-Depends-Indep:
 libjs-jquery <!nodoc>,
# mitmproxy is RC-buggy
# mitmproxy <!nocheck>,
 publicsuffix <!nocheck>,
 python-attr-doc <!nodoc>,
 python-coverage-doc <!nodoc>,
 python-cryptography-doc <!nodoc>,
 python-cssselect-doc <!nodoc>,
 python-itemloaders-doc <!nodoc>,
 python-pytest-doc <!nodoc>,
 python-w3lib-doc <!nodoc>,
 python3-boto3 <!nocheck>,
 python3-botocore <!nocheck>,
 python3-defusedxml (>= 0.7.1) <!nocheck> <!nodoc>,
 python3-doc <!nodoc>,
 python3-itemadapter <!nocheck> <!nodoc>,
 python3-itemloaders <!nocheck> <!nodoc>,
 python3-jmespath <!nocheck>,
 python3-lxml <!nocheck>,
 python3-parsel (>= 1.8.1) <!nocheck> <!nodoc>,
 python3-pexpect <!nocheck>,
 python3-pil <!nocheck>,
 python3-priority <!nocheck>,
 python3-protego <!nocheck>,
 python3-pydispatch <!nocheck> <!nodoc>,
 python3-pyftpdlib <!nocheck>,
 python3-pygments <!nocheck>,
 python3-pytest <!nocheck>,
 python3-queuelib <!nocheck>,
 python3-setuptools,
 python3-sphinx <!nodoc>,
 python3-sphinx-hoverxref <!nodoc>,
 python3-sphinx-notfound-page <!nodoc>,
 python3-sphinx-rtd-theme <!nodoc>,
 python3-sybil <!nocheck>,
 python3-testfixtures (>= 6.10.1) <!nocheck>,
 python3-tldextract <!nocheck> <!nodoc>,
 python3-twisted <!nocheck> <!nodoc>,
 python3-uvloop <!nocheck>,
 python3-w3lib <!nocheck> <!nodoc>,
 sphinx-doc <!nodoc>,
 tox <!nodoc>,
 twisted-doc <!nodoc>,
Standards-Version: 4.7.2
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-scrapy
Vcs-Git: https://salsa.debian.org/python-team/packages/python-scrapy.git
Homepage: https://scrapy.org/
Testsuite: autopkgtest-pkg-pybuild

Package: python-scrapy-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Built-Using: ${sphinxdoc:Built-Using}
Build-Profiles: <!nodoc>
Description: Python web scraping and crawling framework documentation
 Scrapy is a fast high-level screen scraping and web crawling framework,
 used to crawl websites and extract structured data from their pages.
 It can be used for a wide range of purposes, from data mining to
 monitoring and automated testing.
 .
 This package provides the python-scrapy documentation in HTML format.

Package: python3-scrapy
Architecture: all
Depends:
 python3-lxml,
 python3-pydispatch,
 ${misc:Depends},
 ${python3:Depends},
Recommends:
 ipython3,
 python3-brotli,
 python3-pil,
Suggests:
 python3-boto3,
 python3-botocore,
 python-scrapy-doc (= ${binary:Version}),
Description: Python web scraping and crawling framework (Python 3)
 Scrapy is a fast high-level screen scraping and web crawling framework,
 used to crawl websites and extract structured data from their pages.
 It can be used for a wide range of purposes, from data mining to
 monitoring and automated testing.
 .
 This package provides the scrapy module for Python 3.
